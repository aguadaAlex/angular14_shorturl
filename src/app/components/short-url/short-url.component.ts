import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ShortUrlService } from 'src/app/services/short-url.service';

@Component({
  selector: 'app-short-url',
  templateUrl: './short-url.component.html',
  styleUrls: ['./short-url.component.css']
})
export class ShortUrlComponent implements OnInit {

  nombreUrl: string;
  urlShort: string;
  urlProcesada:Boolean;
  loading:Boolean;
  mostrarError:Boolean;
  textError:string;
  constructor(private _shortUrlService:ShortUrlService) { 
    this.nombreUrl='';
    this.urlShort= '';
    this.urlProcesada=false;
    this.loading=false;
    this.mostrarError=false;
    this.textError='';
  }

  ngOnInit(): void {
  }

  procesarUrl() {

    // VALIDAR SI LA URL ES VACIA
    if(this.nombreUrl === ''){
      this.error('Por favor ingrese una URL')
      return;
    }
    this.urlProcesada=false;
    this.loading=true;
    setTimeout(()=>{
      this.obtenerUrlShort();
    },2000);
    
  }
  obtenerUrlShort(){
    this._shortUrlService.getUrlShort(this.nombreUrl).subscribe(data =>{
      this.loading=false;
      console.log(data);
      this.urlProcesada=true;
      this.urlShort=data.link;
    }, error =>{
      this.loading = false;
      this.nombreUrl='';
      if(error.error.description==='The value provided is invalid.'){
        this.error('la URL ingresada es invalida')
      }
    })

  }
   
  error(valor: string){
    this.mostrarError = true;
      this.textError = valor;
      // MOSTRAMOS ERROR POR 4 SEGUNDOS 
      setTimeout(()=>{
        this.mostrarError=false;
      },4000)
  }

}
