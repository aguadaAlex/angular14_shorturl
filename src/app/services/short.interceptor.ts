import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ShortInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const TOKEN = 'f189979c58ce13a96af7007b6a6e153f65d71cd3';
    request = request.clone({ setHeaders: {Authorization: 'Bearer '+ TOKEN}});
    return next.handle(request);
  }
}
